import { test, expect } from '@playwright/test'

test('Клик на кнопку увеличивает цифру', async ({ page }) => {
  await page.goto('/')

  const button = page.getByTestId('cy-button')

  await expect(button).toBeEnabled()
  await button.click()
  await expect(button).toHaveText('count is 1')
})

test('Клик на кнопку Fetch получает и рендерит список', async ({ page }) => {
  await page.goto('/')

  const responsePromise = page.waitForResponse(
    resp =>
      resp.url() === 'https://jsonplaceholder.typicode.com/users' &&
      resp.status() === 200
  )

  const button = page.getByTestId('cy-button2')
  await button.click()
  await responsePromise

  await expect(page.getByTestId('cy-users').getByRole('listitem')).toHaveCount(
    10
  )
})

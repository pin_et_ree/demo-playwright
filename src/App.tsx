import { useState } from 'react'
import './App.css'

type TUser = {
  id: number
  name: string
}

function App() {
  const [count, setCount] = useState(0)
  const [users, setUsers] = useState<TUser[]>([])

  const fetchUsers = () =>
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => setUsers(json))

  return (
    <div className='App'>
      <div className='card'>
        <button onClick={() => setCount((count) => count + 1)} data-qa='cy-button'>
          count is {count}
        </button>
      </div>

      <div className='card'>
        <button onClick={fetchUsers} data-qa='cy-button2'>Fetch</button>
        <ul data-qa='cy-users'>
          {
            users.map(user => <li key={user.id}>{user.id} - {user.name}</li>)
          }
        </ul>
      </div>
    </div>
  )
}

export default App
